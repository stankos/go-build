FROM golang:VERSION

RUN apk --update upgrade; \
    apk add --update git openssh wget curl bash docker docker-compose; \
    rm -rf /var/cache/apk

# install golint
RUN go get -u golang.org/x/lint/golint
# install glide
RUN wget -q -O - https://github.com/Masterminds/glide/releases/download/v0.13.3/glide-v0.13.3-linux-amd64.tar.gz | tar -zxf - && cp linux-amd64/glide /usr/local/bin/glide

ENV CGO_ENABLED=0
